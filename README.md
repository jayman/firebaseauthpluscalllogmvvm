# Android  coding challenge
## This is a demo project for the practical round. It contains two modules:

1. Firebase Auth with Phone Otp
2. Call log details in tree extended list view
## What I have used
I have used a hybrid MVVM code structure with adapter, observable, and state event design pattern.
For dependency Injection i have used Dagger Hilt , 
I have not used any kind of internal storage. I just tried to emit the latest data from the repository.
I have focused more on the functionality and code rather than the UI. So, the UI might not be great, but it is workable.
## Instructions
As we know, if we constantly use Firebase Auth to get OTP SMS, the device may be blocked sometimes. So, I have added one test user: +11234567890 and the OTP is 123456.
The login page works perfectly fine, but the UI is not good. You can see in the shared video how to test it. I have managed the OTP and phone verification on a single page instead of two pages.

## Things that can be improved in this task

## Due to limited time and resources, I wish I could have worked on the following points:

I could have made two separate pages for the login module: 1) Enter mobile number (send OTP) and 2) OTP verification.
If I had more time, I would have extended my architecture further by connecting the common event flow with UI-based event design pattern. However, due to lack of time, I have used standalone normal interfaces and live data.
I have already implemented pagination adapter, but I did not get a chance to connect it with my tree adapter.
I need to make some more extensions, such as for the visibility of views (visible or gone). However, I have already made some of the necessary extensions.
## Note
I have not attached my credit card with the Firebase account as it is a test version. So, there are some limitations, such as the number of authentications in a particular time and the device can be blocked as per the Firebase documentation.

I hope this is helpful! Let me know if you have any other questions.

