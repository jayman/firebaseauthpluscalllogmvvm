package com.apps.jaymandemoforacespritech

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Appclass() : Application() {
}