package com.apps.jaymandemoforacespritech.data

import android.app.Application
import android.content.ContentResolver
import android.database.SQLException
import android.os.Bundle
import android.provider.CallLog
import android.util.Log
import androidx.paging.PositionalDataSource
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class CallLogDataSource(private val application: Application) :
    PositionalDataSource<CallLogEntry>() {
    override fun loadInitial(
        params: LoadInitialParams,
        callback: LoadInitialCallback<CallLogEntry>
    ) {
        Log.d(TAG, "loadInitial()")
        callback.onResult(getContacts(params.requestedLoadSize, params.requestedStartPosition), 0)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<CallLogEntry>) {
        Log.d(TAG, "loadRange()")
        callback.onResult(getContacts(params.loadSize, params.startPosition))
    }

    private fun getContacts(limit: Int, offset: Int): List<CallLogEntry> {
        Log.d(TAG, "getContacts()")
        val calllogs: MutableList<CallLogEntry> = ArrayList()
        //try with resource
        try {
            val bundle = Bundle()
            bundle.putInt(ContentResolver.QUERY_ARG_LIMIT, limit)
            bundle.putInt(ContentResolver.QUERY_ARG_OFFSET, offset)


             application.contentResolver.query(
                CallLog.Calls.CONTENT_URI,
                null,
                null,
                null,
                CallLog.Calls.DATE + " DESC",
                null
            ).use { cursor ->

                //calllogs.
                cursor!!.moveToFirst()
                Log.d(TAG, "cursor count = " + cursor.count)
                while (!cursor.isAfterLast || calllogs.size ?: 0 <= 50 ) {
                    cursor?.let {
                        val id = cursor?.getColumnIndex(CallLog.Calls._ID)?.let { cursor.getInt(it) }
                        val phoneNumber = it?.getColumnIndex(CallLog.Calls.NUMBER)
                            ?.let { it1 -> cursor.getString(it1) } ?:""
                        val type = cursor?.getColumnIndex(CallLog.Calls.TYPE)
                            ?.let { it1 -> cursor.getString(it1) }
                        val contactName =
                            cursor?.getColumnIndex(CallLog.Calls.CACHED_NAME)
                                ?.let { it1 -> cursor.getString(it1) }

                        val userName = cursor?.getColumnIndex(CallLog.Calls.CACHED_NAME)
                            ?.let { it1 -> cursor.getString(it1) } ?:phoneNumber


                        val date = cursor?.getColumnIndex(CallLog.Calls.DATE)?.let { cursor.getLong(it) } ?: 0L

                        val duration = cursor?.getColumnIndex(CallLog.Calls.DURATION)
                            ?.let { it1 -> cursor.getString(it1) }

                        val dircode = type?.toInt()

                      val dir=  when (dircode) {
                            CallLog.Calls.OUTGOING_TYPE ->  "OUTGOING"
                            CallLog.Calls.INCOMING_TYPE ->  "INCOMING"
                            CallLog.Calls.MISSED_TYPE ->  "MISSED"
                            else->"PRESENTATION_UNAVAILABLE"
                        }
                        val str=dir.toString()+" dircode="+dircode+"duration ="+duration+" date= $date"+"type $type"+
                                "phone $phoneNumber"+"id "+id ?:""
                        Log.e(TAG, "getContacts: Data->>"+ str.toString())

                        ///////
                        val callDayTime = Date(java.lang.Long.valueOf(date))
                        val df: DateFormat = SimpleDateFormat("MM/dd/yyyy")
                        //date = df.format(callDayTime)

                        try {
                            calllogs.add( CallLogEntry(id = id.toString(),
                                number = phoneNumber!!,
                            type=dir!!,date=date,duration=duration!!,
                            dateString = df.format(callDayTime),
                            name = userName))
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        Log.d("CallLogDataSource", "" + calllogs[calllogs.size - 1])
                    }

                    cursor.moveToNext()
                }
            }
        } catch (e: SQLException) {
            Log.d(TAG, "" + e)
        }
        return calllogs
    }

    companion object {
        private const val TAG = "CallLogDataSource"
    }
}