package com.apps.jaymandemoforacespritech.data

import android.app.Application
import android.util.Log
import androidx.paging.DataSource

class CallLogDataSourceFactory(application: Application) : DataSource.Factory<Int, CallLogEntry>() {
    private val TAG = "DataSourceFactory"
    private val application: Application

    init {
        Log.d(TAG, "CallLogDataSourceFactory-")
        this.application = application
    }

    override fun create(): DataSource<Int, CallLogEntry> {
        Log.d(TAG, "create()")
        return CallLogDataSource(application)
    }

}