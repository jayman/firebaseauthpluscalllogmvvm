package com.apps.jaymandemoforacespritech.data

data class CallLogEntry(
    val id :String="",
    val number: String="",
    val type: String="",
    val date: Long=0,
    val duration: String="",
    val dateString: String="",
    val name:String=""
)
    fun CallLogEntry.toEmptyObj():CallLogEntry{
        return CallLogEntry(
            id="",
            number="",
            type="",
            date=0L,
            duration="",
            dateString="",
            name=""
        )
    }

