package com.apps.jaymandemoforacespritech.data.repository

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList

import com.apps.jaymandemoforacespritech.data.CallLogDataSourceFactory
import com.apps.jaymandemoforacespritech.data.CallLogEntry

class CallLogRepository(application: Application) {
    var callLogsLiveData: LiveData<PagedList<CallLogEntry>>
    private val PAGE_SIZE = 50

    init {
        val dataSourceFactory = CallLogDataSourceFactory(application)
        val pConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(PAGE_SIZE)
            .build()

        callLogsLiveData= LivePagedListBuilder(dataSourceFactory, pConfig)
          //  .setFetchExecutor(Executors.newSingleThreadExecutor())
            .build()

    }
}