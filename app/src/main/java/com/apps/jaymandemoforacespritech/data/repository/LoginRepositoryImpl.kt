package com.apps.jaymandemoforacespritech.data.repository

import android.app.Activity

import com.apps.jaymandemoforacespritech.util.FireBaseAuthProvider
import com.apps.jaymandemoforacespritech.util.PhoneCallbacksListener
import com.google.firebase.auth.*
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(
val auth :FirebaseAuth,
val fireBaseAuthProvider:FireBaseAuthProvider
) : LoginRepositoty, PhoneCallbacksListener {
    private lateinit var mCallbacksListener:RepositoryCallbacksListener


    init {
            fireBaseAuthProvider.setPhoneCallbacksListener(this)
            if (fireBaseAuthProvider.isUserVerified()) {

            }

    }


    override fun setPhoneCallbacksListener(listener: RepositoryCallbacksListener) {
        this.mCallbacksListener = listener

    }

   override fun getOptByMobile(phone: String,activity: Activity) :Unit {
      fireBaseAuthProvider.sendVerificationCode(phone,activity)
/*
       val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber("+917779012796") // Phone number to verify
            .setTimeout(20L, TimeUnit.SECONDS) // Timeout and unit

            .setCallbacks(
                object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {

                        //Getting the code sent by SMS
                        val code = phoneAuthCredential.smsCode
                        Log.e("TAG", "onVerificationCompleted: " + code)
                        //sometime the code is not detected automatically
                        //in this case the code will be null
                        //so user has to manually enter the code
                        if (code != null) {

                            //verifyVerificationCode(code)
                            callback1?.invoke(
                                Resource.Success(
                                    LoginUserData(
                                        authToken = "", currentUser = null,
                                        otp = code
                                    )
                                )
                            )

                        }
                    }

                    override fun onVerificationFailed(e: FirebaseException) {
                        //Toast.makeText(this@LoginActivity, e.message, Toast.LENGTH_LONG).show()
                        Log.e("TAG", e.message.toString())

                        callback1?.invoke(Resource.Failure(e.localizedMessage?.toString() ?: ""))
                    }

                    //when the code is generated then this method will receive the code.
                    override fun onCodeSent(
                        s: String,
                        forceResendingToken: PhoneAuthProvider.ForceResendingToken
                    ) {
                        // super.onCodeSent(s, forceResendingToken)

                        //storing the verification id that is sent to the user
                        //mVerificationId = s
                        callback1?.invoke(
                            Resource.Success(
                                LoginUserData(
                                    authToken = s, currentUser = null,
                                    otp = code
                                )
                            )
                        )

                    }
                }) // OnVerificationStateChangedCallbacks

            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)*/
    }

    override  fun verifyPhone(otp: String) {
        try {
            signInWithPhoneAuthCredential(fireBaseAuthProvider.verifyVerificationCode(otp))

        } catch (e: Exception) {

        }
    }
    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                mCallbacksListener.onOtpVerifyCompleted()
            } else {
                when (it.exception) {
                    is FirebaseAuthInvalidCredentialsException -> {
                        mCallbacksListener.onOtpVerifyFailed("Wrong Otp")
                    }
                }
            }
        }
    }

    override fun onVerificationCompleted() {
      mCallbacksListener.onVerificationCompleted()
    }

    override fun onVerificationCodeDetected(code: String) {
        Timber.d("AuthActivityViewModel onReceive: success $code")
      //  selectedOtpNumber.value = code
        mCallbacksListener.onVerificationCodeDetected(code)
    }

    override fun onVerificationFailed(message: String) {
        //viewInteractor?.showSnackBarMessage(message)
        mCallbacksListener.onVerificationFailed(message)
    }

    override fun onCodeSent(
        verificationId: String?,
        token: PhoneAuthProvider.ForceResendingToken?
    ) {
        mCallbacksListener.onCodeSent(verificationId,token)
        //viewInteractor?.showSnackBarMessage("OTP has sent")
    }


    interface RepositoryCallbacksListener {
        fun onVerificationCompleted()
        fun onVerificationCodeDetected(code: String)
        fun onVerificationFailed(message: String)
        fun onCodeSent(
            verificationId: String?,
            token: PhoneAuthProvider.ForceResendingToken?
        )
        fun onOtpVerifyCompleted()
        fun onOtpVerifyFailed(message:String)
    }
}