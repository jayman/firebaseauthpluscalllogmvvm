package com.apps.jaymandemoforacespritech.data.repository

import android.app.Activity
import com.apps.jaymandemoforacespritech.data.Resource

interface LoginRepositoty {
    fun setPhoneCallbacksListener(listener: LoginRepositoryImpl.RepositoryCallbacksListener)
    fun getOptByMobile(phone:String,activity:Activity) :Unit
     fun verifyPhone(mobile:String) :Unit
}