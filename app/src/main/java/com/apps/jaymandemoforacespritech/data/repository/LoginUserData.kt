package com.apps.jaymandemoforacespritech.data.repository

import com.google.firebase.auth.FirebaseUser

data class LoginUserData(
    val authToken :String,
    val currentUser:FirebaseUser?,
    val otp:String=""
)
