package com.apps.jaymandemoforacespritech.di

import com.apps.jaymandemoforacespritech.data.repository.LoginRepositoryImpl
import com.apps.jaymandemoforacespritech.data.repository.LoginRepositoty
import com.apps.jaymandemoforacespritech.util.FireBaseAuthProvider
import com.google.firebase.auth.FirebaseAuth
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

@Provides
@Singleton
fun provideFirebaseAuthInstance():FirebaseAuth {

    return FirebaseAuth.getInstance().also {
       // it.firebaseAuthSettings.forceRecaptchaFlowForTesting(false)
        //it.firebaseAuthSettings.setAppVerificationDisabledForTesting(true)

    }
}
  @Provides
  @Singleton
  fun provideFireBaseAuthProvider(auth: FirebaseAuth) : FireBaseAuthProvider{
      return FireBaseAuthProvider(auth)
  }

  @Provides
  @Singleton
  fun provideLoginRespository(auth: FirebaseAuth, provider:FireBaseAuthProvider):LoginRepositoty{
      return LoginRepositoryImpl(auth,provider)
  }

}