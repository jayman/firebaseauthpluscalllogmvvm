package com.apps.jaymandemoforacespritech.ui.MainPage

import android.media.browse.MediaBrowser.ItemCallback
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.apps.jaymandemoforacespritech.R
import com.apps.jaymandemoforacespritech.data.CallLogEntry
import com.apps.jaymandemoforacespritech.util.toMMDDYY

class CallLogAdapter (): PagedListAdapter<CallLogEntry, CallLogAdapter.ViewHolder>(NewsDiffCallback) {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)  {
        var phoneNo:TextView
        var dateView:TextView
        var callType:TextView
        init {
            phoneNo=itemView.findViewById(R.id.phone_no)
            dateView=itemView.findViewById(R.id.date_time)
            callType=itemView.findViewById(R.id.call_type)
        }

    }


    companion object{

        val NewsDiffCallback = object : DiffUtil.ItemCallback<CallLogEntry>() {
            override fun areItemsTheSame(oldItem: CallLogEntry, newItem: CallLogEntry): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: CallLogEntry, newItem: CallLogEntry): Boolean {
                return oldItem == newItem
            }
        }
}

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentData=getItem(position)
        holder.phoneNo.text=currentData?.number
        holder.callType.text=currentData?.type
        holder.dateView.text=currentData?.date?.toMMDDYY()


    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Inflate the custom layout
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_call_log, parent, false)
        return ViewHolder(view)
    }

}