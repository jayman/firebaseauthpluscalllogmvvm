package com.apps.jaymandemoforacespritech.ui.MainPage

import android.Manifest
import android.Manifest.permission.READ_CALL_LOG
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.CallLog
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.apps.jaymandemoforacespritech.R
import com.apps.jaymandemoforacespritech.data.CallLogEntry
import com.apps.jaymandemoforacespritech.databinding.ActivityHomeBinding
import com.github.adriankuta.expandable_recyclerview.ExpandableTreeNode
import com.github.adriankuta.expandable_recyclerview.expandableTree
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.util.*

class HomeActivity : AppCompatActivity() {
    private val PHONE_LOG_PERMISSION = 1
    private val viewModel: HomeViewModel by viewModels()
    lateinit var binding:ActivityHomeBinding
    lateinit var callAdapter:CallLogAdapter
    lateinit var treeAdapter:ExpandableAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        callAdapter=CallLogAdapter()
        treeAdapter= ExpandableAdapter()

        checkCallLogPermission()
        observeData()
        binding.recCallList.apply {
            this.layoutManager= LinearLayoutManager(this@HomeActivity)
            this.adapter=treeAdapter
        }
    }

    private fun observeData() {

        viewModel.getCallLog().observe(this@HomeActivity , Observer{pagedList->
            val list=pagedList.toList()
            val calendar = GregorianCalendar.getInstance()
            val groups = list.groupBy { item ->
               // val date = format.parse(item.timeString)
                calendar.setTime(Date(item.date))
                calendar.get(Calendar.DATE)
                item.dateString.trim().toString()
            }
            var rootDate = ExpandableTreeNode(CallLogEntry())

            groups.forEach {
                val key =it.key
                val value=it.value
                val rootDate1 = ExpandableTreeNode(CallLogEntry(dateString = key))

                rootDate.addChild(rootDate1)

               val groupByName=value.groupBy { it -> it.name.trim().toString() }
                val finalList= groupByName.forEach { name, callLogEntries ->
                    val child_rootDate_name = ExpandableTreeNode(CallLogEntry(name=name))
                    rootDate1.addChild(child_rootDate_name)
                    callLogEntries?.forEach {
                        entry->
                        child_rootDate_name.addChild(ExpandableTreeNode(entry))

                    }

                }

            }
            Log.e("TAG", "RootData: "+rootDate.prettyString() )
            treeAdapter.setTree(rootDate)

            Log.e("TAG", "GROUPED: "+groups?.toString() )

            Timber.e("GROUPED >>"+list.toString())
            callAdapter.submitList(pagedList)
            Log.e("TAG", "observeData: "+pagedList?.toList()?.toString() )
        })
    }

    private fun checkCallLogPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CALL_LOG
            ) != PackageManager.PERMISSION_GRANTED

        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_CALL_LOG),
                PHONE_LOG_PERMISSION
            )
            return
        }else{
            //val logs=  getCallLogs(this@HomeActivity)

          //  Log.e("TAG", "onRequestPermissionsResult: logs= "+logs.toString() )
        }


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PHONE_LOG_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted

                observeData()
              //  Log.e("TAG", "onRequestPermissionsResult: logs= "+logs.toString() )
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)


    }




}