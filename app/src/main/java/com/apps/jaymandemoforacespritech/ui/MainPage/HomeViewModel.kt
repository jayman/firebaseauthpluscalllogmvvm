package com.apps.jaymandemoforacespritech.ui.MainPage

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.apps.jaymandemoforacespritech.data.repository.CallLogRepository
import dagger.hilt.android.lifecycle.HiltViewModel


class HomeViewModel(application: Application) : AndroidViewModel(application) {
    private val callLogRepository: CallLogRepository

    init {
        callLogRepository = CallLogRepository(application)

    }
    fun getCallLog() =  callLogRepository.callLogsLiveData
}