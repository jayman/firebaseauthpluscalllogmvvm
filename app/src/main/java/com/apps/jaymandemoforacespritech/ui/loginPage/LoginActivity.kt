package com.apps.jaymandemoforacespritech.ui.loginPage

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.apps.jaymandemoforacespritech.databinding.ActivityLoginBinding
import com.apps.jaymandemoforacespritech.ui.MainPage.HomeActivity
import com.apps.jaymandemoforacespritech.ui.MainPage.HomeViewModel
import com.apps.jaymandemoforacespritech.util.isValidMobile
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.TimeUnit


private lateinit var binding:ActivityLoginBinding
private var user: FirebaseUser ?=null
private val auth by lazy {
    FirebaseAuth.getInstance()
}
var mVerificationId:String=""

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private val viewModel: LoginViewModel by viewModels()
    private val PHONE_LOG_PERMISSION = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //
        user=FirebaseAuth.getInstance().currentUser ?: null

        if (user!=null){
            //auth success move to home
            Log.e("TAG", "onCreate: User"+ user?.toString() )
            //auth.signOut()
            Toast.makeText(this,"Already Authenticate Welcome Back !",Toast.LENGTH_LONG).show()
            startActivity(Intent(this,HomeActivity::class.java))
            finish()
        }else{
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_CALL_LOG
                ) != PackageManager.PERMISSION_GRANTED

            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_CALL_LOG),
                    PHONE_LOG_PERMISSION
                )
                return
            }
        }

        binding.edOtp.isVisible=false
        binding.edPhone.isVisible=true
        binding.buttonPhone.isVisible=true
        binding.button.isVisible=false

        //get otp
        binding.buttonPhone.setOnClickListener {
            if (binding.edPhone.text.toString().isValidMobile()){
                binding.layLoader.isVisible = true
                // if wanted to use dynamic number then use below string
                val enterNo="+91"+binding.edPhone.text.toString().trim()
                viewModel.getOtpByMobile("+11234567890",this@LoginActivity)

                //verifyEnterNo(binding.edPhone.text.toString())
            }else{
                Toast.makeText(this,"Enter valid Number",Toast.LENGTH_LONG).show()
            }

        }
        // after otp authenticate
        binding.button.setOnClickListener {
          if (binding.edOtp.text.toString().isNotEmpty()){
              binding.layLoader.isVisible = true

              viewModel.getVerifyCodeByOtp("123456")

              //verifyVerificationCode(binding.edOtp.text.toString())

          }else{
              Toast.makeText(this,"Enter valid Otp",Toast.LENGTH_LONG).show()
          }
        }

       viewModel.selectedOtpNumber.observe(this, Observer {
           code->
           binding.layLoader.isVisible = true

           viewModel.getVerifyCodeByOtp(code)
       })
      viewModel.errorMessage.observe(this, Observer {

          binding.layLoader.isVisible = false

          Toast.makeText(this,it,Toast.LENGTH_LONG).show()
      })
      viewModel.otpSend.observe(this, Observer {
          if (it){
              Toast.makeText(this@LoginActivity,"OTP SENT !",Toast.LENGTH_LONG).show()
              binding.layLoader.isVisible = false

              binding.edOtp.isVisible=true
              binding.edPhone.isVisible=true

              binding.buttonPhone.isVisible=true
              binding.button.isVisible=true
          }
      })
      viewModel.finalProcessCompleted.observe(this, Observer {
          if (it){
              binding.layLoader.isVisible = false

              Toast.makeText(this,"Authenticate SuccessFully !",Toast.LENGTH_LONG).show()

              startActivity(Intent(this,HomeActivity::class.java))
              finish()
          }

      })
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PHONE_LOG_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
              Timber.e("Permission Granted ")
            } else {
               // Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)


    }
}