package com.apps.jaymandemoforacespritech.ui.loginPage

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apps.jaymandemoforacespritech.data.repository.LoginRepositoryImpl
import com.apps.jaymandemoforacespritech.data.repository.LoginRepositoty
import com.google.firebase.auth.PhoneAuthProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
@HiltViewModel
class LoginViewModel @Inject constructor(
private val repository:LoginRepositoty
) :ViewModel(), LoginRepositoryImpl.RepositoryCallbacksListener {
init {
    repository.setPhoneCallbacksListener(this)
}

   //start getting otp sms process
    fun getOtpByMobile(phoneNo:String,activity:Activity){
        repository.getOptByMobile(phoneNo,activity)
    }

   //final verification base on otp
    fun getVerifyCodeByOtp(otp: String) {
        repository.verifyPhone(otp)
    }

    sealed class LoginEvent(){
        object Loading :LoginEvent()
        data class SuccesOtp(val string: String):LoginEvent()
        data class ErrorOtp(val string: String):LoginEvent()
        data class SuccessPhone(val string: String):LoginEvent()
        data class ErrorPhone(val string: String):LoginEvent()

    }
    private var _selectedOtpNumber = MutableLiveData<String>()
    val selectedOtpNumber: LiveData<String> = _selectedOtpNumber

   private var _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    private var _finalProcessCompleted = MutableLiveData<Boolean>()
    val finalProcessCompleted: LiveData<Boolean> = _finalProcessCompleted

    private var _otpSend = MutableLiveData<Boolean>()
    val otpSend: LiveData<Boolean> = _otpSend

    override fun onVerificationCompleted() {
        // phone verified
    }

    override fun onVerificationCodeDetected(code: String) {
        _selectedOtpNumber.postValue(code)
    }

    override fun onVerificationFailed(message: String) {
       //
        _errorMessage.postValue(message)
    }

    override fun onCodeSent(
        verificationId: String?,
        token: PhoneAuthProvider.ForceResendingToken?
    ) {
        //otp has sent process start
        _otpSend.postValue(true)

    }

    override fun onOtpVerifyCompleted() {
        _finalProcessCompleted.postValue(true)
    }

    override fun onOtpVerifyFailed(message: String) {
       _errorMessage.postValue(message)
    }
}