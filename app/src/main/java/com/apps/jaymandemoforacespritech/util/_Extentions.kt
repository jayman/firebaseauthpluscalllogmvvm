package com.apps.jaymandemoforacespritech.util

import com.google.android.gms.tasks.Task
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.suspendCancellableCoroutine
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.coroutines.resumeWithException

fun String.isValidMobile() :Boolean{
    // The regular expression to match a valid mobile number.
    val regex = "^[1-9]\\d{9}$"

    // Match the regular expression against the phone number.
    val matcher = Regex(regex).matchEntire(this)

    // Return true if the phone number matches the regular expression, false otherwise.
    return matcher != null
}

fun Long.toMMDDYY():String{
    val date=  Date(this)
    val df = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
    return df.format(date)
}

fun Long.toHHmmss():String{
    val date=  Date(this)
    val df = SimpleDateFormat("HH:mm:ss")
    return df.format(date)
}

@OptIn(ExperimentalCoroutinesApi::class)
suspend fun <T> Task<T>.await(): T {
    return suspendCancellableCoroutine { cont ->
        addOnCompleteListener {
            if (it.exception != null) {
                cont.resumeWithException(it.exception!!)
            } else {
                cont.resume(it.result, null)
            }
        }
    }
}